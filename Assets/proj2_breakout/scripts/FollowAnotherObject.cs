using UnityEngine;
using System.Collections;

public class FollowAnotherObject : MonoBehaviour {

	
	public float fromCam = 11.0f;
	
	void Update () {
		
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Vector3 pos = ray.GetPoint(fromCam);	
		transform.position = pos;
		
	}
}
