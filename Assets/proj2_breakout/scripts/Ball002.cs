using UnityEngine;
using System.Collections;


public class Ball002 : MonoBehaviour {

	// first, follow the paddle
	// when mouse is clicked, add force, remove paddle follow
	// do not add more force after first mouse click
	// make sure force is sustained through
	
/* 
access paddle (variable)
ball follower(function)
mouseClick (event)
disable follower (function)
force amount (variable)
add force (function)
normalize force (function)
*/

public GameObject bumperBox;

public float speed = 10.0f;
	
public Vector3 point;
public Vector3 velocity;


	
	void Start () {
	
	bumperBox = GameObject.Find("bumperBox");
	print ("I found object: " + bumperBox);


		}


	void Update () {
		
		bool ballfired = false;
	
		if(ballfired = false && Input.GetMouseButton(0)){	
			FollowAnotherObject FollowScript = GetComponent<FollowAnotherObject>();
			FollowScript.enabled = false;
			print("No longer following, shut down " + FollowScript);
			rigidbody.AddForce(Vector3.back * speed);
			ballfired = true;
		}		
		
		if(velocity.magnitude < speed)
		{
			print ("Adding Force");
			rigidbody.AddForce(Vector3.back * speed); 
		}

		if(velocity.magnitude > 0)
		{
		point = transform.InverseTransformPoint(new Vector3(0, -0.2f, 0));
		velocity = rigidbody.GetPointVelocity(point);
		print ("velocity is: " + velocity.magnitude);
		}
		
	}
	
}

