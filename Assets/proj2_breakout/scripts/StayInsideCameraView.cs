using UnityEngine;
using System.Collections;

public class StayInsideCameraView : MonoBehaviour {
	
// public Transform target;	

	
	void Start () {
	
	}
	
	
	void Update () {
/*			
Vector3 Pos = camera.WorldToViewportPoint(target.position)	
		if(Pos.x < 0.0f)
			print("target is out on the left side!");
		else if(Pos.x > 1.0f)
			print("target is out on the right side!");
		else if(Pos.y > 1.0f)
			print("target is out up");
		else if(Pos.y < 0.0f)
			print("target is out down");
		else
			print("target is in bounds");
*/
	Vector3 v = rigidbody.velocity;
	Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
	Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
	
	
	if (topRight.y > 1) 
		{
			rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
		}
	else if (bottomLeft.y < 0) 
		{
			rigidbody.velocity = new Vector3(v.x, Mathf.Abs(v.y), v.z);
		}
	
	
	if (topRight.x < 0 || bottomLeft.x > 1) 
		{
			transform.position = Vector3.zero;	
		}
}
}

